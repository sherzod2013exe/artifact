package pro.fatmoney.mytrainer.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "files_info")
data class DataInfo(
        @ColumnInfo(name = "file_url")
        var fileUrl: String,
        @ColumnInfo(name = "file_path")
        var filePath: String,
        @ColumnInfo(name = "workout_id")
        var workoutId: Int,
        @ColumnInfo(name = "user_id")
        var userId: Int,
        @ColumnInfo(name = "amz_upload")
        var amzUploaded: Int,
        @ColumnInfo(name = "device_hand")
        var deviceHand: String,
        @ColumnInfo(name = "device_foot")
        var deviceFoot: String?,
        @ColumnInfo(name = "train_type")
        var trainType: String
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_")
    var id: Long? = null
}