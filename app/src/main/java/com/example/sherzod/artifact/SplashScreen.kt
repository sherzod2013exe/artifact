package com.example.sherzod.artifact

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import com.example.sherzod.artifact.R.drawable.vasa
import kotlinx.android.synthetic.main.content_splash_scrin.*


class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT < 16) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash_scrin)
        val pref = PrefManager
        val secondsDelayed = 3
//        Glide.with(this).load(ResourcesCompat.getDrawable(resources, R.drawable.vasa, null)).into(vasa)

        Handler().postDelayed({
            if(pref.LANGUAGE.equals(null))
                startActivity(Intent(this, MainActivity::class.java))
                else
            startActivity(Intent(this, LanguageActivity::class.java))
            finish()
        }, (secondsDelayed * 1000).toLong())
    }

}
