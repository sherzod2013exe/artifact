package com.example.sherzod.artifact.network

import io.reactivex.Observable
import retrofit2.http.*
import java.util.*

interface TrainerService {
    @Multipart
    @POST("https://en.wikipedia.org/w/api.php")
    fun auth(
        @Query("format") format: String, @Query("action") action: String,
        @Query("prop") prop: String, @Query("exintro") exintro: String,
        @Query("explaintext") explaintext: String, @Query("titles"
        ) title: String
    ): Any


}