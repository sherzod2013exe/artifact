package com.example.sherzod.artifact

import android.content.Context
import android.content.SharedPreferences

class PrefManager(context: Context){
    private val pref:SharedPreferences

    init {
        pref = context.getSharedPreferences(PREF_NAME,0)
    }

    fun id() = pref.getString(ID_KEY,null)

    fun language() = pref.getString(LANGUAGE,null)

    fun saveLanguage(language:String) = pref.edit().putString(LANGUAGE,language).apply()

    fun saveToken(token:String) = pref.edit().putString(TOKEN,token).apply()

    companion object {
        const val PREF_NAME = "settings"
        const val ID_KEY = "id"
        const val TOKEN = "token"
        const val LANGUAGE = "language"

    }
}