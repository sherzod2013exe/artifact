package com.example.sherzod.artifact

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.Window
import android.view.WindowManager
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_language.*

class LanguageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_language)
        val pref = PrefManager(this)
        russian.setOnClickListener{
            pref.saveLanguage((it as TextView).text.toString())
            startActivity()
        }
        english.setOnClickListener{
            pref.saveLanguage((it as TextView).text.toString())
            startActivity()
        }
        french.setOnClickListener{
            pref.saveLanguage((it as TextView).text.toString())
            startActivity()
        }
        spain.setOnClickListener{
            pref.saveLanguage((it as TextView).text.toString())
            startActivity()
        }
    }
    fun startActivity(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

}
