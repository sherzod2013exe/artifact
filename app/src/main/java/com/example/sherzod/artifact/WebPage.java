package com.example.sherzod.artifact;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;

public class WebPage extends Activity {

    WebView webView;
    TextToSpeech TTS;
    Boolean position = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_layout);
        String page_url = getIntent().getStringExtra("page_url");
        webView  = new WebView(this);

        final ImageButton button = findViewById(R.id.play);
        TTS = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override public void onInit(int initStatus) {
                if (initStatus == TextToSpeech.SUCCESS) {
                    if (TTS.isLanguageAvailable(new Locale(Locale.getDefault().getLanguage()))
                            == TextToSpeech.LANG_AVAILABLE) {
                        TTS.setLanguage(new Locale(Locale.getDefault().getLanguage()));
                    } else {
                        TTS.setLanguage(Locale.US);
                    }
                    TTS.setPitch(1.3f);
                    TTS.setSpeechRate(0.7f);
                } else if (initStatus == TextToSpeech.ERROR) {
                }
            }
        });
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        TTS.speak("A sword is a bladed weapon intended for slashing or thrusting that is longer than a knife or dagger, consisting of a long blade attached to a hilt. The precise definition of the term varies with the historical epoch or the geographic region under consideration. The blade can be straight or curved. Thrusting swords have a pointed tip on the blade, and tend to be straighter; slashing swords have a sharpened cutting edge on one or both sides of the blade,",TextToSpeech.QUEUE_FLUSH,map);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position){
                    button.setImageResource(R.drawable.ic_pause_black_24dp);
                    position = true;
                }else {
                    button.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    position = false;
                }
//                t1.speak("A sword is a bladed weapon intended for slashing or thrusting that is longer than a knife or dagger, consisting of a long blade attached to a hilt. The precise definition of the term varies with the historical epoch or the geographic region under consideration. The blade can be straight or curved. Thrusting swords have a pointed tip on the blade, and tend to be straighter; slashing swords have a sharpened cutting edge on one or both sides of the blade, and are more likely to be curved. Many swords are designed for both thrusting and slashing.\n" +
//                        "\n" +
//                        "Historically, the sword developed in the Bronze Age, evolving from the dagger; the earliest specimens date to about 1600 BC. The later Iron Age sword remained fairly short and without a crossguard. The spatha, as it developed in the Late Roman army, became the predecessor of the European sword of the Middle Ages, at first adopted as the Migration Period sword, and only in the High Middle Ages, developed into the classical arming sword with crossguard. The word sword continues the Old English, sweord.", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        webView.getSettings().setJavaScriptEnabled(true); // enable javascript

        final Activity activity = this;

        webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

        webView.loadUrl(page_url);
        setContentView(webView);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
