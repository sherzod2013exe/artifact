package com.example.sherzod.artifact

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.JsonObjectRequest
import com.example.sherzod.artifact.adapter.WikiSearchAdapter
import com.example.sherzod.artifact.models.Result
import com.example.sherzod.artifact.remote.MyApplication
import com.example.sherzod.artifact.remote.SuggestionsService

import kotlinx.android.synthetic.main.activity_about.*
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class AboutActivity : AppCompatActivity() {

    val url : String? = null
    val SUGGESTIONS = "FetchSuggestions"
    private val TAG = "MainActivity"
    private var mNewData: ArrayList<Result>? = null
    private var mOldData:ArrayList<Result>? = null
    lateinit var searchAdapter: WikiSearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        val intent1 = intent
        val extra = intent1.getStringExtra("url")
        registerMyReceiver()
        mNewData = ArrayList()
        mOldData = ArrayList()
        searchAdapter = WikiSearchAdapter(this, R.id.sgst_list, mNewData)
        sgst_list.adapter = searchAdapter
        startServiceForSuggestion(extra)
        sgst_list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, l ->
            val result = getItem(position)
            if (result != null) {
                fetchFull_URL(result.pageId)
            }
        }

    }

    private fun getItem(position: Int): Result? {
        return if (mNewData!!.size >= position) {
            mNewData!![position]
        } else {
            null
        }
    }


    private val mServiceReciever = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val response = intent.getStringExtra("response")
            getResultArray(response)
            mNewData!!.clear()
            mNewData!!.addAll(mOldData!!)
            searchAdapter.notifyDataSetChanged()
        }
    }

    private fun getResultArray(response: String): ArrayList<Result> {
        mOldData!!.clear()
        try {
            Log.v("Response", response)
            val jsonObject = JSONObject(response)
            val jsonArray = jsonObject.getJSONObject("query").getJSONArray("pages")
            for (i in 0 until jsonArray.length()) {
                val result = Result()
                val jsonObject1 = jsonArray.get(i) as JSONObject
                val page_id = jsonObject1.getString("pageid")
                var thumbnail = ""
                val title = jsonObject1.getString("title")
                try {
                    val thumbnail_obj = jsonObject1.getJSONObject("thumbnail")
                    thumbnail = thumbnail_obj.getString("source")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                var desc = ""
                try {
                    desc = jsonObject1.getJSONObject("terms").getString("description")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                result.pageId = page_id
                result.title = title
                result.thumbnail = thumbnail
                result.desc = desc
                mOldData!!.add(result)
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return mOldData!!
    }

    private fun registerMyReceiver() {
        try {
            val intentFilter = IntentFilter()
            intentFilter.addAction(SUGGESTIONS)
            registerReceiver(mServiceReciever, intentFilter)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun fetchFull_URL(pageId: String) {
        val url = getString(R.string.page_url) + "&prop=info&inprop=url&format=json&pageids=" + pageId
        Log.v(TAG, url)
        val jsonObjectRequest2 = JsonObjectRequest(Request.Method.GET, url, null,
            Response.Listener { response ->
                try {
                    val page_url =
                        response.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId)
                            .getString("fullurl")
                    val browser_intent = Intent(this, WebPage::class.java)
                    browser_intent.putExtra("page_url", page_url)
                    startActivity(browser_intent)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener {
                Toast.makeText(
                    this,
                    "Unable to Find Page",
                    Toast.LENGTH_SHORT
                ).show()
            })
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest2)

    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected

    }

    private fun startServiceForSuggestion(search_tag: String) {
        if (!isNetworkAvailable()) {
            Toast.makeText(
                this, "No Network Available",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            val intent1 = Intent(this, SuggestionsService::class.java)
            intent1.putExtra("search_tag", search_tag)
            startService(intent1)
        }

    }

    companion object {
        fun startActivity(activity: Activity,data: String) {
            val intent = Intent(activity, AboutActivity::class.java)
            intent.putExtra("url",data)
            activity.startActivity(intent)
            activity.finish()
        }
    }
}
