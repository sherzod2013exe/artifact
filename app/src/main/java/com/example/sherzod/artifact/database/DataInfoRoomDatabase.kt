package pro.fatmoney.mytrainer.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.migration.Migration
import com.example.sherzod.artifact.database.DataInfoDao

@Database(entities = [DataInfo::class],version = 2)
abstract class DataInfoRoomDatabase:RoomDatabase(){
    abstract fun dataInfoDao(): DataInfoDao
    companion object {
        val MIGRATION_1_2 = object: Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE files_info ADD train_type TEXT NOT NULL DEFAULT 'USUAL_TRAINING'")
            }
        }
    }
}