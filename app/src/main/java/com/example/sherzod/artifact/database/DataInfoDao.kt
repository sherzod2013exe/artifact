package com.example.sherzod.artifact.database

import android.arch.persistence.room.*
import pro.fatmoney.mytrainer.database.DataInfo

@Dao
interface DataInfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(dataInfo: DataInfo)

    @Query("SELECT * FROM files_info WHERE amz_upload=0")
    fun getNotLoaded():List<DataInfo>

    @Query("SELECT * FROM files_info WHERE amz_upload=1")
    fun getLoaded() : List<DataInfo>

    @Delete
    fun delete(dataInfo: DataInfo)

    @Update
    fun update(dataInfo: DataInfo)
}